//basename() function
<?php
$path = "/farhana_146905_b34_session5/test.php";

//Show filename with file extension
echo basename($path) ."<br/>";

//Show filename without file extension
echo basename($path,".php");
?>


//dirname() function
<?php
echo dirname("c:/farhana_146905_b34_session5/test.php") . "<br />";
echo dirname("/farhana_146905_b34_session5/test.php");
?>


//fopen() function
<?php
$filename = "test.txt";
$file = fopen( $filename, "r" );
?>

//fclose() function
<?php
$file = fopen("test.txt","r");
//some code to be executed
fclose($file);
?>

//file_get_contents function
<?php
$homepage = file_get_contents('http://www.google.com/');
echo $homepage;
?>

//filesize() function
<?php
echo filesize("test.txt");
echo "<br>";
?>
<?php
echo filetype("test.txt");
?>

//fread() function
<?php
$file = fopen("test.txt","r");
fread($file,"10");
fclose($file);
?>
//Read entire file:
<?php
$file = fopen("test.txt","r");
fread($file,filesize("test.txt"));
fclose($file);
?>


